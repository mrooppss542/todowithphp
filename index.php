<?php include "db.php";?>
<?php 
    $query="select * from todo";
    $result=mysqli_query($connection,$query);

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $todo=$_POST['todo'];
        $date=date('l dS F\, Y');
        $sql="INSERT INTO todo(T_todo,T_date) VALUES('$todo','$date');";
        $results=mysqli_query($connection,$sql);

        if(!$results){
            die("Falied");
        }
        else{
            header("Location:index.php?todo-added");
        }
    }

    if(isset($_GET['delete_todo'])){
        $dt_id=$_GET['delete_todo'];
        $sql_delete="DELETE FROM todo WHERE T_id=$dt_id";
        $res=mysqli_query($connection,$sql_delete);

        if(!$res){
            die("FAILED");
        }
        else{
            header("Location:index.php?todo-deleted");
        }
    }
?>
<html>
<head>
    <title>Todo App</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">   
</head>
<body>
    <div class="container">
    <div class="todo">
    <h1>Todo Application</h1>
    <h3>Add a new Todo</h3>
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">    
            <div class="form-group">
                <input name="todo" placeholder="Name of the task Todo" type="text" class="form-control">
            </div>    
            <div class="form-group">
                <input class="btn btn-primary" value="Add a new todo task list" type="submit">
            </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <th>ID</th>
                <th>Todo</th>
                <th>Date Added</th>
                <th>Edit Todo</th>
                <th>Delete Todo</th>
            </thead>
            <tbody>
            <?php
                while ($row =mysqli_fetch_assoc($result)){
                    #echo $row['T_id'];
                    $t_id=$row['T_id'];
                    $t_todo=$row['T_todo'];
                    $t_date=$row['T_date'];
            ?>
                <tr>
                    <td><?php echo $t_id; ?></td>
                    <td><?php echo $t_todo; ?></td>
                    <td><?php echo $t_date; ?></td>
                    <td><a href="edit.php?edit_todo=<?php echo $t_id;?>" class="btn btn-primary">Edit Todo</a></td>
                    <td><a href="index.php?delete_todo=<?php echo $t_id;?>" class="btn btn-danger">Delete Todo</a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    </div>
</body>
</html> 